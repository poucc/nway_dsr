package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"runtime"
	"strings"
)

var NWAYWORD string //
func GetCurrentDirectory() string {
	dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		println(err)
	}
	if runtime.GOOS == "linux" {
		dir += "/"
	} else {
		dir += "\\"
	}
	return dir

}
func LoadKeyWord() {
	ks, err := ioutil.ReadFile(GetCurrentDirectory() + "./keywords.ini")
	if err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}
	NWAYWORD = strings.Trim(string(ks), " ")

}

/*
“
*/
const DSTRING = `user=postgres dbname=nwaydsr password=Nway2017 host=127.0.0.1 port=5432 sslmode=disable`
