package export_excel

import (
	"github.com/tealeg/xlsx"
	"os"
	"github.com/astaxie/beego/utils"
)

func ExportExcel(list []map [string]interface{}) (filename string, err error) {

	var file *xlsx.File
	var sheet *xlsx.Sheet
	var row *xlsx.Row
	var cell *xlsx.Cell

	file = xlsx.NewFile()
	sheet, _ = file.AddSheet("sheet1")
	for _,v:=range list{
		if v["ai_speak"] == "" || v["people_speak"] == ""{
			continue
		}
		row = sheet.AddRow()
		if v["ai_speak"] != "-1"{//a路
			cell = row.AddCell()
			cell.Value = v["ai_speak"].(string)

			cell = row.AddCell()
			cell.Value = ""
		}else {
			cell = row.AddCell()
			cell.Value = ""

			cell = row.AddCell()
			cell.Value = v["people_speak"].(string)
		}


	}

	if !utils.FileExists("upload") {
		os.MkdirAll("upload", os.ModePerm)
	}
	filename = "upload/file.xlsx"
	err = file.Save(filename)
	return filename, err
}
