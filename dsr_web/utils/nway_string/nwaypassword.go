package nway_string

import (
	"crypto/rand"
)

func MakePassword(length int) (string, error) {
	b := make([]byte, length)
	_, err := rand.Read(b)
	if err != nil {
		return "", err
	}
	return string(b), nil
}
