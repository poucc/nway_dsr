package nway_string

import (
	"strings"
	"unicode"
)

func IsDigits(str string) bool {
	for _, r := range str {
		if unicode.IsDigit(r) == false {
			return false
		}

	}
	return true
}

func GetValidString(s string) string {

	bytes := []byte(s)
	for i, ch := range bytes {

		switch {
		case ch > '~':
			bytes[i] = ' '
		case ch == '\r':
		case ch == '\n':
		case ch == '\t':
		case ch < ' ':
			bytes[i] = ' '
		}
	}
	return strings.Trim(string(bytes), " ")
}
func GetBetweenStr(str, start, end string) string {
	n := strings.Index(str, start)
	if n == -1 {
		n = 0
	}
	str = string([]byte(str)[n+len(start):])
	m := strings.Index(str, end)
	if m == -1 {
		m = len(str)
	}
	str = string([]byte(str)[:m])
	return str
}
