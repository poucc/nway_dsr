package nway_path

import (
	"os"
	"path/filepath"
	"runtime"
)

func GetCurrentDirectory() string {
	dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		println(err)
	}
	if runtime.GOOS == "linux" {
		dir += "/"
	} else {
		dir += "\\"
	}
	return dir
	//println(dir)
	//return strings.Replace(dir, "\\", "/", -1)
}
