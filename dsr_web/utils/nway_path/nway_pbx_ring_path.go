package nway_path

import (
	"bytes"
	"math/rand"
	"os"
	"path"
	"runtime"
	"strconv"
	"time"
)

func RandomString(l int) string {
	var result bytes.Buffer
	var temp string
	for i := 0; i < l; {
		temp = string(RandInt(65, 90))
		result.WriteString(temp)
		i++

	}
	return result.String()
}

func RandInt(min int, max int) int {
	return min + rand.Intn(max-min)
}
func MakeRecordPath() string {
	var filename string
	now := time.Now()
	filename = strconv.Itoa(now.Year()) + strconv.Itoa(int(now.Month())) + strconv.Itoa(now.Day())
	filename += strconv.Itoa(now.Hour()) + strconv.Itoa(now.Minute()) + strconv.Itoa(now.Second())
	filename += RandomString(6) + ".wav"

	return filename
}
func MakeRingPath(basepath, prefix string, ext string, platform string) (string, string) {
	var nwaypath string
	now := time.Now()
	nwaypath = basepath + prefix
	os.MkdirAll(nwaypath, 777)
	filename := strconv.Itoa(now.Year()) + strconv.Itoa(int(now.Month())) + strconv.Itoa(now.Day())
	filename += strconv.Itoa(now.Hour()) + strconv.Itoa(now.Minute()) + strconv.Itoa(now.Second())
	filename += RandomString(6) + ext
	nwaypath += filename
	return nwaypath, filename
}
func MakeTmpFilePath(basepath, filename string) string {
	var spt string = "\\"

	if runtime.GOOS == "linux" {
		spt = "/"
	}
	now := time.Now()
	nwaypath := basepath + spt + "Tmp" + spt
	os.MkdirAll(nwaypath, 777)
	fileSuffix := path.Ext(filename)
	newfilename := strconv.Itoa(now.Year()) + strconv.Itoa(int(now.Month())) + strconv.Itoa(now.Day())
	newfilename += strconv.Itoa(now.Hour()) + strconv.Itoa(now.Minute()) + strconv.Itoa(now.Second())
	newfilename += RandomString(6) + fileSuffix
	nwaypath += newfilename
	return nwaypath
}
