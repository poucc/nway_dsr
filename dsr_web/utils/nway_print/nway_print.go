package nwayprt

import (
	"fmt"
	"path"
	"runtime"
	"strconv"
	"time"

	//"github.com/astaxie/beego"
	"ai/ai_control/utils/log"
)

var fmt_level int = 1

func Nway_println(v ...interface{}) error {
	when := time.Now()
	_, file, line, ok := runtime.Caller(2)
	if !ok {
		file = "???"
		line = 0
	}
	_, filename := path.Split(file)
	msg := "[" + filename + ":" + strconv.FormatInt(int64(line), 10) + "] Debug:"
	//beego.Debug(v)
	logger.Debug(v)
	if fmt_level == 0 {
		fmt.Println(when, msg, v)

		//return err
	}
	return nil
}
func Nway_err_println(v ...interface{}) error {
	when := time.Now()
	_, file, line, ok := runtime.Caller(2)
	if !ok {
		file = "???"
		line = 0
	}
	_, filename := path.Split(file)
	msg := "[" + filename + ":" + strconv.FormatInt(int64(line), 10) + "] Error:"
	if fmt_level == 0 {
		_, err := fmt.Println(when, msg, v)
		logger.Error(v)
		return err
	}
	return nil
}
