package models

import (
	"fmt"
	"strconv"

	"github.com/astaxie/beego/orm"
)

type DsrCdr struct {
	Id            int64
	Account_id    int64
	Callee        string
	Caller        string
	Start_time    string
	End_time      string
	Duration      int
	Record_base   string
	Record_path   string
	Call_id       string
	A_answer_time string
	B_answer_time string
	A_billsec     int
	B_billsec     int
	B_caller      string
	B_callee      string
	Other_uuid    string
	A_score       int
	B_score       int
	Score         int
	Offline_qc    bool
}
type Runtime struct {
	Id       int64
	Call_id  string
	A_number string
	B_number string
}

const cdrsql = "dsr_cdr"
const runtimesql = "runtime"

func CdrGetList(page, pageSize int, filters ...string) ([]*DsrCdr, int64) {

	offset := (page - 1) * pageSize
	list := make([]*DsrCdr, 0)
	query := orm.NewOrm().QueryTable(cdrsql)

	//过滤 条件。
	if len(filters) > 0 {
		fmt.Println(filters[0], filters[1])
		for i := 0; i < len(filters); i += 2 {
			query = query.Filter(filters[i], filters[i+1])
		}
	}

	total, _ := query.Count()
	var err error
	if page == -1 {
		_, err = query.OrderBy("-id").All(&list)
	} else {
		_, err = query.OrderBy("-id").Limit(pageSize, offset).All(&list)
	}
	if err != nil {
		ErrorLog(err)
	}
	return list, total

}

func CdrGetList1(page, pageSize int, name string, filters ...string) ([]*DsrCdr, int64) {
	offset := (page - 1) * pageSize
	sqlStr := "SELECT distinct dsr_cdr.* FROM dsr_cdr," + name + " WHERE " + name + ".call_id=dsr_cdr.other_uuid"
	for i := 0; i < len(filters); i += 2 {
		if filters[i] == "start_time__icontains" {
			sqlStr += " and UPPER(dsr_cdr.start_time::text) LIKE UPPER('%" + filters[i+1] + "%')"
		}
		if filters[i] == "start_time__lte" {
			sqlStr += " and dsr_cdr.start_time <= '" + filters[i+1] + "'"
		}
		if filters[i] == "end_time__gte" {
			sqlStr += " AND dsr_cdr.end_time >= '" + filters[i+1] + "'"
		}
		if filters[i] == "callee__exact" {
			sqlStr += " AND dsr_cdr.callee = '" + filters[i+1] + "'"
		}
		if filters[i] == "caller__exact" {
			sqlStr += " AND dsr_cdr.caller = '" + filters[i+1] + "'"
		}
		if filters[i] == "a_billsec__gte" {
			sqlStr += " AND dsr_cdr.a_billsec >= '" + filters[i+1] + "'"
		}
		if filters[i] == "b_billsec__gte" {
			sqlStr += " AND dsr_cdr.b_billsec >= '" + filters[i+1] + "'"
		}
	}
	sqlStr += "  ORDER BY id DESC LIMIT " + strconv.Itoa(pageSize)
	if page != -1 {
		sqlStr += " OFFSET " + strconv.Itoa(offset)
	}
	var list []*DsrCdr
	count, err := orm.NewOrm().Raw(sqlStr).QueryRows(&list)
	if err != nil {
		ErrorLog(err)
		return list, count
	}
	fmt.Println(list)
	return list, count
}

func GetAllRuntime() []*Runtime {

	list := make([]*Runtime, 0)
	query := orm.NewOrm().QueryTable(runtimesql)
	var err error
	_, err = query.OrderBy("-id").All(&list)
	if err != nil {
		ErrorLog(err)

	}
	return list
}

func CdrCountInfo(filters ...interface{}) int64 {
	query := orm.NewOrm().QueryTable(cdrsql)
	//过滤 条件。
	if len(filters) > 0 {
		fmt.Println(filters[0].(string), filters[1])
		for i := 0; i < len(filters); i += 2 {
			query = query.Filter(filters[i].(string), filters[i+1])
		}
	}
	count, _ := query.Count()
	return count
}

func KeyCountInfo(sql string, filters ...interface{}) int64 {
	query := orm.NewOrm().QueryTable(sql)
	//过滤 条件。
	if len(filters) > 0 {
		fmt.Println(filters[0].(string), filters[1])
		for i := 0; i < len(filters); i += 2 {
			query = query.Filter(filters[i].(string), filters[i+1])
		}
	}
	count, _ := query.Count()
	return count
}
func IsRuntimeByCallId(callid string) (bool, error) {

	sqlStr := "SELECT count(1) FROM runtime WHERE call_id='" + callid + "'"
	var flowCount int
	err := orm.NewOrm().Raw(sqlStr).QueryRow(&flowCount)
	if err != nil {
		ErrorLog(err)
		return false, err
	}
	if flowCount != 0 {
		return true, nil
	}
	return false, nil
}
func InsertCdr(callid string) error {

	sqlStr := "insert into dsr_cdr (call_id,other_uuid,offline_qc) values ('" + callid + "','" + callid + "',true);"
	_, err := orm.NewOrm().Raw(sqlStr).Exec()
	if err != nil {
		ErrorLog(err)
		return err
	}
	return nil
}
