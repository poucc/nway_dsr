package models

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	"github.com/astaxie/beego/logs"
	_ "github.com/lib/pq"

)

func Init() {
	pgstr := beego.AppConfig.String("dbstring")
	orm.RegisterDataBase("default", "postgres", pgstr)
	orm.RegisterModel(new(DsrCdr),new(DsrFlowContent),new(DsrUser),
		new(FailKeys),new(FineKeys),new(SuccessKeys),new(FailInfo),new(FineInfo),new(Runtime),new(SuccessInfo))

	if beego.AppConfig.String("runmode") == "dev" {
		orm.Debug = true
	}
	//配置日志
	logs.SetLogger(logs.AdapterFile, `{"filename":"`+beego.AppConfig.String("log_file")+`"}`)
}

func ErrorLog(err error )  {
	logs.Error(err)
}