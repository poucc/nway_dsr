package models

import (
	"fmt"
	"strconv"

	"github.com/astaxie/beego/orm"
)

type FailKeys struct {
	Id          int64
	Keyword     string
	Description string
	Score       int
	AutoHangup  bool
}

type FineKeys struct {
	Id          int64
	Keyword     string
	Description string
	Score       int
}

type SuccessKeys struct {
	Id          int64
	Keyword     string
	Description string
}

type FailInfo struct {
	Id      int64
	D_time  string
	Call_id string
	Content string
}

type FineInfo struct {
	Id      int64
	Call_id string
	D_time  string
	Content string
}

type SuccessInfo struct {
	Id      int64
	Call_id string
	D_time  string
	Content string
}

const failSql = "fail_keys"
const successSql = "success_keys"
const fineSql = "fine_keys"

func AddFailKeys(a *FailKeys) (int64, error) {
	return orm.NewOrm().Insert(a)
}

func AddFineKeys(a *FineKeys) (int64, error) {
	return orm.NewOrm().Insert(a)
}

func AddSuaaessKeys(a *SuccessKeys) (int64, error) {
	return orm.NewOrm().Insert(a)
}

func KeywordGetById(id int64, ktype string) (*FailKeys, *FineKeys, *SuccessKeys, error) {
	var err error
	F := new(FailKeys)
	fine := new(FineKeys)
	S := new(SuccessKeys)
	fmt.Println(ktype)
	if ktype == "1" {
		err = orm.NewOrm().QueryTable(failSql).Filter("id", id).One(F)
	} else if ktype == "2" {
		err = orm.NewOrm().QueryTable(fineSql).Filter("id", id).One(fine)
	} else if ktype == "3" {
		err = orm.NewOrm().QueryTable(successSql).Filter("id", id).One(S)
	}
	if err != nil {
		return nil, nil, nil, err
	}
	return F, fine, S, nil
}

func (a *FailKeys) Delete() error {
	if _, err := orm.NewOrm().Delete(a); err != nil {
		return err
	}
	return nil
}

func (a *FineKeys) Delete() error {
	if _, err := orm.NewOrm().Delete(a); err != nil {
		return err
	}
	return nil
}

func (a *SuccessKeys) Delete() error {
	if _, err := orm.NewOrm().Delete(a); err != nil {
		return err
	}
	return nil
}

// 失败的列表
func FailGetList(page, pageSize int, filters ...interface{}) ([]*FailKeys, int64) {

	offset := (page - 1) * pageSize
	list := make([]*FailKeys, 0)
	query := orm.NewOrm().QueryTable(failSql)
	//过滤 条件。
	if len(filters) > 0 {
		fmt.Println(filters[0].(string), filters[1])
		for i := 0; i < len(filters); i += 2 {
			query = query.Filter(filters[i].(string), filters[i+1])
		}
	}
	total, _ := query.Count()
	var err error
	if page == -1 {
		_, err = query.OrderBy("id").All(&list)
	} else {
		_, err = query.OrderBy("id").Limit(pageSize, offset).All(&list)
	}
	if err != nil {
		ErrorLog(err)
	}
	return list, total

}

// 优良的列表
func FineGetList(page, pageSize int, filters ...interface{}) ([]*FineKeys, int64) {

	offset := (page - 1) * pageSize
	list := make([]*FineKeys, 0)
	query := orm.NewOrm().QueryTable(fineSql)
	//过滤 条件。
	if len(filters) > 0 {
		fmt.Println(filters[0].(string), filters[1])
		for i := 0; i < len(filters); i += 2 {
			query = query.Filter(filters[i].(string), filters[i+1])
		}
	}
	total, _ := query.Count()
	var err error
	if page == -1 {
		_, err = query.OrderBy("id").All(&list)
	} else {
		_, err = query.OrderBy("id").Limit(pageSize, offset).All(&list)
	}
	if err != nil {
		ErrorLog(err)
	}
	return list, total

}

// 一般的列表
func SuccessGetList(page, pageSize int, filters ...interface{}) ([]*SuccessKeys, int64) {

	offset := (page - 1) * pageSize
	list := make([]*SuccessKeys, 0)
	query := orm.NewOrm().QueryTable(successSql)
	//过滤 条件。
	if len(filters) > 0 {
		fmt.Println(filters[0].(string), filters[1])
		for i := 0; i < len(filters); i += 2 {
			query = query.Filter(filters[i].(string), filters[i+1])
		}
	}
	total, _ := query.Count()
	var err error
	if page == -1 {
		_, err = query.OrderBy("id").All(&list)
	} else {
		_, err = query.OrderBy("id").Limit(pageSize, offset).All(&list)
	}
	if err != nil {
		ErrorLog(err)
	}
	return list, total

}

// 根据id 修改关键字
func UpdateKeywordFromId(keyword, tabname string, id int64) error {

	sql := "UPDATE " + tabname + " set keyword='" + keyword + "' WHERE id=" + strconv.FormatInt(id, 10)
	_, err := orm.NewOrm().Raw(sql).Exec()
	if err != nil {
		ErrorLog(err)
		return err
	}
	return nil

}

// 根据id 修改关键字
func UpdateKeywordScoreFromId(score, tabname string, id int64) error {

	sql := "UPDATE " + tabname + " set score=" + score + " WHERE id=" + strconv.FormatInt(id, 10)
	_, err := orm.NewOrm().Raw(sql).Exec()
	if err != nil {
		ErrorLog(err)
		return err
	}
	return nil

}
func UpdateKeywordAutoHangupFromId(auto_hangup, tabname string, id int64) error {

	sql := "UPDATE " + tabname + " set auto_hangup=" + auto_hangup + " WHERE id=" + strconv.FormatInt(id, 10)
	_, err := orm.NewOrm().Raw(sql).Exec()
	if err != nil {
		ErrorLog(err)
		return err
	}
	return nil

}
