package routers

import (
	"nway_dsr/dsr/controllers"
	"github.com/astaxie/beego"
)

func init() {
    beego.Router("/", &controllers.LoginController{},"*:LoginIn")
	beego.Router("/login_out", &controllers.LoginController{}, "*:LoginOut")
	beego.Router("/login", &controllers.LoginController{}, "*:LoginIn")

	beego.Router("/home", &controllers.BaseController{}, "*:Index")
	beego.Router("/home/start", &controllers.HomeController{}, "*:AdminStart")
	beego.Router("/xiaochengxu", &controllers.BaseController{},"post:XLogin")

	beego.AutoRouter(&controllers.CdrController{})
	beego.AutoRouter(&controllers.HomeController{})
	beego.AutoRouter(&controllers.AdminController{})
	beego.AutoRouter(&controllers.KeywordsController{})
}
