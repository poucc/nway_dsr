package controllers

import (
	"nway_dsr/dsr/models"
	"time"
	"fmt"
)

type HomeController struct {
	BaseController
}

func (self *HomeController) Index() {
	self.Data["pageTitle"] = "系统首页"
	self.TplName = "home/adminstart.html"
}

func (self *HomeController) AdminStart() {
	self.Data["pageTitle"] = "首页"
	filters := make([]interface{}, 0)
	timeStr := time.Now().Format("2006-01-02")
	filters = append(filters, "start_time__icontains", timeStr)
	filters = append(filters, "duration__gte", "1")
	count := models.CdrCountInfo(filters...)
	self.Data["count"] = count

	filters = make([]interface{}, 0)
	filters = append(filters, "start_time__icontains", timeStr)
	count = models.CdrCountInfo(filters...)
	self.Data["all"] = count

	filters = make([]interface{}, 0)
	filters = append(filters, "d_time__icontains", timeStr)
	count = models.KeyCountInfo("fine_info", filters...)
	self.Data["fine_count"] = count

	filters = make([]interface{}, 0)
	filters = append(filters, "d_time__icontains", timeStr)
	count = models.KeyCountInfo("fail_info", filters...)
	self.Data["fail_count"] = count

	runtimeList := models.GetAllRuntime()
	lists := [] map[string]string{}
	for _,v := range runtimeList{
		lists = append(lists, map[string]string{"number":v.A_number,"uuid":v.Call_id})
	}
	self.Data["runtime"] = lists
	self.Data["count"] = len(lists)

	self.display()
}


func (self *HomeController) FlowContentlist() {
	//列表
	runtimeList := models.GetAllRuntime()
	lists := [] map[string]string{}
	for _,v := range runtimeList{
		lists = append(lists, map[string]string{"number":v.A_number,"uuid":v.Call_id})
	}
	self.ajaxList("成功", MSG_OK,0,lists)
}


func (self *HomeController) Import(){
	_,_, err := self.UploadFile("./upload/excel/", "uploadField")
	if err != nil {
		fmt.Println(err)
		self.ajaxMsg("格式错误", MSG_ERR)
	}
	self.ajaxMsg("", MSG_OK)
}


func (self *HomeController) ImportRecord(){
	_ ,uuid, err := self.UploadFile("./upload/record/", "uploadField")
	if err != nil {
		fmt.Println(err)
		self.ajaxMsg("格式错误", MSG_ERR)
	}
	self.ajaxMsg(uuid, MSG_OK)
}

