package controllers

import (
	"fmt"
	"nway_dsr/dsr/models"
)

type KeywordsController struct {
	BaseController
}

func (self *KeywordsController) List() {
	self.Data["pageTitle"] = "关键词列表"
	self.Data["type"] = self.GetString("type")
	self.display()
}

func (self *KeywordsController) AjaxSave() {
	keys_type := self.GetString("type")
	if keys_type == "1" {
		key := new(models.FailKeys)
		key.Keyword = self.GetString("content")
		_, err := models.AddFailKeys(key)
		if err != nil {
			fmt.Println(err)
		}
		self.ajaxMsg("成功", MSG_OK)
	} else if keys_type == "2" {
		key := new(models.FineKeys)
		key.Keyword = self.GetString("content")
		_, err := models.AddFineKeys(key)
		if err != nil {
			fmt.Println(err)
		}
		self.ajaxMsg("成功", MSG_OK)
	} else if keys_type == "3" {
		key := new(models.SuccessKeys)
		key.Keyword = self.GetString("content")
		_, err := models.AddSuaaessKeys(key)
		if err != nil {
			fmt.Println(err)
		}
		self.ajaxMsg("成功", MSG_OK)

	}

}

func (self *KeywordsController) ContentTable() {
	keys_type := self.GetString("type")
	//列表
	page, err := self.GetInt("page")
	if err != nil {
		page = 1
	}
	limit, err := self.GetInt("limit")
	if err != nil {
		limit = 30
	}

	self.pageSize = limit
	//查询条件
	filters := make([]interface{}, 0)
	if keys_type == "1" {
		result, count := models.FailGetList(page, self.pageSize, filters...)
		list := make([]map[string]interface{}, len(result))

		for k, v := range result {
			row := make(map[string]interface{})
			row["id"] = v.Id
			row["keyword"] = v.Keyword
			row["score"] = v.Score
			if v.AutoHangup {
				row["hanup"] = "是"
			} else {
				row["hanup"] = "否"
			}

			list[k] = row
		}
		self.ajaxList("成功", MSG_OK, count, list)
	} else if keys_type == "2" {
		result, count := models.FineGetList(page, self.pageSize, filters...)
		list := make([]map[string]interface{}, len(result))

		for k, v := range result {
			row := make(map[string]interface{})
			row["id"] = v.Id
			row["keyword"] = v.Keyword
			row["score"] = v.Score
			list[k] = row
		}
		self.ajaxList("成功", MSG_OK, count, list)
	} else if keys_type == "3" {
		result, count := models.SuccessGetList(page, self.pageSize, filters...)
		list := make([]map[string]interface{}, len(result))

		for k, v := range result {
			row := make(map[string]interface{})
			row["id"] = v.Id
			row["keyword"] = v.Keyword
			list[k] = row
		}
		self.ajaxList("成功", MSG_OK, count, list)
	}

}

func (self *KeywordsController) UpdateKeyword() {
	Process_id, _ := self.GetInt64("id")
	keyword := self.GetString("keyword")
	tabname := self.GetString("tabname")
	err := models.UpdateKeywordFromId(keyword, tabname, Process_id)

	if err != nil {
		self.ajaxMsg(err, MSG_ERR)
	}
	self.ajaxMsg("成功", MSG_OK)
}

func (self *KeywordsController) UpdateKeywordScore() {
	Process_id, _ := self.GetInt64("id")
	score := self.GetString("score")
	tabname := self.GetString("tabname")
	err := models.UpdateKeywordScoreFromId(score, tabname, Process_id)

	if err != nil {
		self.ajaxMsg(err, MSG_ERR)
	}
	self.ajaxMsg("成功", MSG_OK)
}
func (self *KeywordsController) UpdateKeywordHangup() {
	Process_id, _ := self.GetInt64("id")
	auto_hangup := self.GetString("auto_hangup")
	if auto_hangup != "true" && auto_hangup != "false" {
		self.ajaxMsg(auto_hangup+",请输入true或false", MSG_ERR)
	}
	tabname := self.GetString("tabname")
	err := models.UpdateKeywordAutoHangupFromId(auto_hangup, tabname, Process_id)

	if err != nil {
		self.ajaxMsg(err, MSG_ERR)
	}
	self.ajaxMsg("成功", MSG_OK)
}

// 删除关键词
func (self *KeywordsController) AjaxDel() {

	user_id, _ := self.GetInt64("id")
	var err error
	if self.GetString("type") == "1" {
		fail, _, _, _ := models.KeywordGetById(user_id, "1")
		err = fail.Delete()
	} else if self.GetString("type") == "2" {
		_, fine, _, _ := models.KeywordGetById(user_id, "2")
		err = fine.Delete()
	} else if self.GetString("type") == "3" {
		_, _, success, _ := models.KeywordGetById(user_id, "3")
		err = success.Delete()
	}
	if err != nil {
		self.ajaxMsg(err.Error(), MSG_ERR)
	}
	self.ajaxMsg("删除成功", 0)
}
