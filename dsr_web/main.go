package main

import (
	_ "nway_dsr/dsr/routers"
	"github.com/astaxie/beego"
	"nway_dsr/dsr/models"
)

func main() {
	models.Init()
	beego.SetStaticPath("/vad_dir", "vad_dir")
	beego.SetStaticPath("/upload", "upload")
	beego.Run()
}

