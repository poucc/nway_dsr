package nway_datetime

import (
	"time"
)

import (
	"strconv"
)

func GetCurrentDate() string {
	year := time.Now().Year()
	month := int(time.Now().Month())
	day := time.Now().Day()
	var strmonth, strday string
	if month < 10 {
		strmonth = "0" + strconv.Itoa(month)
	} else {
		strmonth = strconv.Itoa(month)
	}
	if day < 10 {
		strday = "0" + strconv.Itoa(day)
	} else {
		strday = strconv.Itoa(day)
	}
	current_date := strconv.Itoa(year) + "-" + strmonth + "-" + strday
	return current_date
}

func GetCurrentTime() string {
	nwaydate := GetCurrentDate()
	now := time.Now()
	hour := now.Hour()
	minute := now.Minute()
	second := now.Second()
	var strhour, strminute, strsecond string
	if hour < 10 {
		strhour = "0" + strconv.Itoa(hour)
	} else {
		strhour = strconv.Itoa(hour)
	}
	if minute < 10 {
		strminute = "0" + strconv.Itoa(minute)
	} else {
		strminute = strconv.Itoa(minute)
	}
	if second < 10 {
		strsecond = "0" + strconv.Itoa(second)
	} else {
		strsecond = strconv.Itoa(second)
	}
	nwaydate += " " + strhour + ":" + strminute + ":" + strsecond
	return nwaydate
}
