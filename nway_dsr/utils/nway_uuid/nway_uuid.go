package nway_uuid

import (
	"github.com/satori/go.uuid"
	"fmt"
)


func GetUUId() string{
	// Creating UUID Version 4
	// panic on error
	u1 := uuid.Must(uuid.NewV4())
	fmt.Printf("UUIDv4: %s\n", u1)
	return u1.String()
}