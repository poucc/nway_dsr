package main

import (
	_ "log"

	. "nway_dsr/nway_dsr/nway_fs_ctrl"

	"nway_dsr/nway_dsr/utils/log"
	"nway_dsr/nway_dsr/utils/nway_db_connection"
	"nway_dsr/nway_dsr/utils/nway_path"
	"os"
	"runtime"

	"github.com/astaxie/beego/config"
)

func main() {
	Channels = 2000

	runtime.GOMAXPROCS(runtime.NumCPU())
	logger.SetConsole(false)

	logger.SetRollingFile(nway_path.GetCurrentDirectory(), "nwaydsr.log", 300, 50, logger.MB)

	logger.Info("Starting Nway service\n")
	logger.Debug("Debug info\n")

	logger.Info("This is From http://www.nway.com.cn")
	iniconf, errc := config.NewConfig("ini", "Nway.conf")
	logger.SetLevel(logger.INFO)
	if errc != nil {

		logger.Error("config file fatal", errc)
		return
	}
	dbstring := iniconf.String("dbstring")
	NwayConn := nwayconnection.NewDb()
	bConnected := NwayConn.Init(dbstring)
	logger.Info(dbstring)
	if bConnected == true {
		logger.Info("Init database successed")
		NwayConn.GetConn()
	} else {
		logger.Error("connect to database failed,close Nway_pbx")

		os.Exit(0)

	}
	ASR = iniconf.String("asr")
	APPID = iniconf.String("kd_appid")
	logger.Debug("use asr:", ASR)
	err := StartInboundService()
	if err != nil {
		logger.Error("start service failed:  ", err)
	}
	logger.Debug("Wait for Thread terminal")

}
