--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.10
-- Dumped by pg_dump version 9.6.10

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: dsr_cdr; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.dsr_cdr (
    id bigint NOT NULL,
    account_id bigint DEFAULT 0,
    callee character varying(50) DEFAULT ''::character varying,
    caller character varying(50) DEFAULT ''::character varying,
    start_time timestamp without time zone DEFAULT now(),
    end_time timestamp without time zone DEFAULT now(),
    duration integer DEFAULT 0,
    record_base character varying(128) DEFAULT ''::character varying,
    record_path character varying(128) DEFAULT ''::character varying,
    call_id character varying(128) DEFAULT ''::character varying,
    a_answer_time timestamp without time zone DEFAULT now(),
    b_answer_time timestamp without time zone DEFAULT now(),
    a_billsec integer DEFAULT 0,
    b_billsec integer DEFAULT 0,
    b_caller character varying(50) DEFAULT ''::character varying,
    b_callee character varying(50) DEFAULT ''::character varying,
    other_uuid character varying(50) DEFAULT ''::character varying,
    a_score integer DEFAULT 0,
    b_score integer DEFAULT 0,
    score integer DEFAULT 0,
    offline_qc boolean DEFAULT false
);


ALTER TABLE public.dsr_cdr OWNER TO postgres;

--
-- Name: COLUMN dsr_cdr.account_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.dsr_cdr.account_id IS '用户id';


--
-- Name: COLUMN dsr_cdr.callee; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.dsr_cdr.callee IS '被叫号码';


--
-- Name: COLUMN dsr_cdr.caller; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.dsr_cdr.caller IS '外显号码';


--
-- Name: COLUMN dsr_cdr.start_time; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.dsr_cdr.start_time IS '开始时间';


--
-- Name: COLUMN dsr_cdr.end_time; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.dsr_cdr.end_time IS '结束时间';


--
-- Name: COLUMN dsr_cdr.duration; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.dsr_cdr.duration IS '通话时长';


--
-- Name: COLUMN dsr_cdr.record_base; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.dsr_cdr.record_base IS '语音路径';


--
-- Name: COLUMN dsr_cdr.record_path; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.dsr_cdr.record_path IS '语音路径';


--
-- Name: COLUMN dsr_cdr.a_score; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.dsr_cdr.a_score IS 'a评分';


--
-- Name: COLUMN dsr_cdr.b_score; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.dsr_cdr.b_score IS 'b评分';


--
-- Name: COLUMN dsr_cdr.score; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.dsr_cdr.score IS '总评分';


--
-- Name: ai_cdr_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.ai_cdr_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ai_cdr_id_seq OWNER TO postgres;

--
-- Name: ai_cdr_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.ai_cdr_id_seq OWNED BY public.dsr_cdr.id;


--
-- Name: call_pg_cdr_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.call_pg_cdr_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.call_pg_cdr_id_seq OWNER TO postgres;

--
-- Name: call_pg_cdr; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.call_pg_cdr (
    id integer DEFAULT nextval('public.call_pg_cdr_id_seq'::regclass) NOT NULL,
    local_ip_v4 character varying(16) DEFAULT ' '::character varying,
    caller_id_name character varying(50) DEFAULT ' '::character varying,
    caller_id_number character varying(50) DEFAULT ' '::character varying,
    outbound_caller_id_number character varying(50) DEFAULT ' '::character varying,
    destination_number character varying(50) DEFAULT ' '::character varying,
    context character varying(50) DEFAULT ' '::character varying,
    start_stamp timestamp without time zone,
    answer_stamp timestamp without time zone,
    end_stamp timestamp without time zone,
    duration integer DEFAULT 0,
    billsec integer DEFAULT 0,
    hangup_cause character varying(30) DEFAULT ' '::character varying,
    uuid character varying(50) DEFAULT ' '::character varying,
    bleg_uuid character varying(50) DEFAULT ' '::character varying,
    accountcode character(50) DEFAULT ' '::bpchar,
    read_codec character(10) DEFAULT ' '::bpchar,
    write_codec character(10) DEFAULT ' '::bpchar,
    record_file character varying(255) DEFAULT ' '::character varying,
    direction character varying(50) DEFAULT ' '::character varying,
    sip_hangup_disposition character varying(50),
    origination_uuid character varying(100),
    sip_gateway_name character varying(50),
    sip_term_status character varying(50) DEFAULT ''::character varying,
    sip_term_cause character varying(50) DEFAULT ''::character varying
);


ALTER TABLE public.call_pg_cdr OWNER TO postgres;

--
-- Name: dsr_flow_content; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.dsr_flow_content (
    id bigint NOT NULL,
    call_id character varying(128) DEFAULT ''::character varying,
    content character varying(128) DEFAULT ''::character varying,
    record_path character varying(300) DEFAULT ''::character varying,
    record_base character varying(300) DEFAULT ''::character varying,
    callee character varying DEFAULT ''::character varying,
    caller character varying DEFAULT ''::character varying,
    dsr_time timestamp without time zone DEFAULT now(),
    fine character varying DEFAULT ''::character varying,
    fail character varying DEFAULT ''::character varying,
    speak_second integer DEFAULT 0
);


ALTER TABLE public.dsr_flow_content OWNER TO postgres;

--
-- Name: COLUMN dsr_flow_content.id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.dsr_flow_content.id IS 'id';


--
-- Name: COLUMN dsr_flow_content.callee; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.dsr_flow_content.callee IS '被叫';


--
-- Name: COLUMN dsr_flow_content.caller; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.dsr_flow_content.caller IS '主叫';


--
-- Name: COLUMN dsr_flow_content.fine; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.dsr_flow_content.fine IS '优良词汇';


--
-- Name: COLUMN dsr_flow_content.fail; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.dsr_flow_content.fail IS '敏感词汇';


--
-- Name: dsr_seat; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.dsr_seat (
    id bigint NOT NULL
);


ALTER TABLE public.dsr_seat OWNER TO postgres;

--
-- Name: dsr_user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.dsr_user (
    id bigint NOT NULL,
    account character varying(20) NOT NULL,
    password character varying(32) NOT NULL,
    name character varying(32) NOT NULL,
    salt character varying(32),
    create_time bigint NOT NULL
);


ALTER TABLE public.dsr_user OWNER TO postgres;

--
-- Name: COLUMN dsr_user.account; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.dsr_user.account IS '账号';


--
-- Name: COLUMN dsr_user.password; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.dsr_user.password IS '密码';


--
-- Name: COLUMN dsr_user.name; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.dsr_user.name IS '账号名字';


--
-- Name: COLUMN dsr_user.salt; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.dsr_user.salt IS '密码盐';


--
-- Name: COLUMN dsr_user.create_time; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.dsr_user.create_time IS '创建时间';


--
-- Name: dsr_user_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.dsr_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dsr_user_id_seq OWNER TO postgres;

--
-- Name: dsr_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.dsr_user_id_seq OWNED BY public.dsr_user.id;


--
-- Name: fail_keys; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.fail_keys (
    id bigint NOT NULL,
    keyword character varying(100) NOT NULL,
    description text DEFAULT ''::text,
    score integer DEFAULT 0,
    auto_hangup boolean DEFAULT false
);


ALTER TABLE public.fail_keys OWNER TO postgres;

--
-- Name: COLUMN fail_keys.score; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.fail_keys.score IS '分数';


--
-- Name: fail_info_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.fail_info_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.fail_info_id_seq OWNER TO postgres;

--
-- Name: fail_info_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.fail_info_id_seq OWNED BY public.fail_keys.id;


--
-- Name: fail_info; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.fail_info (
    id bigint DEFAULT nextval('public.fail_info_id_seq'::regclass) NOT NULL,
    call_id character varying(100) DEFAULT ''::character varying,
    content character varying(300) DEFAULT ''::character varying,
    d_time timestamp without time zone DEFAULT now(),
    is_read boolean DEFAULT false
);


ALTER TABLE public.fail_info OWNER TO postgres;

--
-- Name: fail_keys_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.fail_keys_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.fail_keys_id_seq OWNER TO postgres;

--
-- Name: fail_keys_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.fail_keys_id_seq OWNED BY public.fail_keys.id;


--
-- Name: fine_info_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.fine_info_id_seq
    START WITH 2
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.fine_info_id_seq OWNER TO postgres;

--
-- Name: fine_info; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.fine_info (
    id bigint DEFAULT nextval('public.fine_info_id_seq'::regclass) NOT NULL,
    call_id character varying(100) DEFAULT ''::character varying,
    content character varying(300) DEFAULT ''::character varying,
    d_time timestamp without time zone DEFAULT now(),
    is_read boolean DEFAULT false
);


ALTER TABLE public.fine_info OWNER TO postgres;

--
-- Name: fine_keys; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.fine_keys (
    id bigint NOT NULL,
    keyword character varying(100) NOT NULL,
    description text DEFAULT ''::text,
    score integer DEFAULT 0
);


ALTER TABLE public.fine_keys OWNER TO postgres;

--
-- Name: COLUMN fine_keys.score; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.fine_keys.score IS '分数';


--
-- Name: fine_keys_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.fine_keys_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.fine_keys_id_seq OWNER TO postgres;

--
-- Name: fine_keys_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.fine_keys_id_seq OWNED BY public.fine_keys.id;


--
-- Name: flow_content_id_sqe; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.flow_content_id_sqe
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.flow_content_id_sqe OWNER TO postgres;

--
-- Name: flow_content_id_sqe; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.flow_content_id_sqe OWNED BY public.dsr_flow_content.id;


--
-- Name: runtime; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.runtime (
    id bigint NOT NULL,
    call_id character varying(50) DEFAULT ''::character varying,
    a_number character varying(50) DEFAULT ''::character varying,
    b_number character varying(50) DEFAULT ''::character varying
);


ALTER TABLE public.runtime OWNER TO postgres;

--
-- Name: runtime_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.runtime_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
    CYCLE;


ALTER TABLE public.runtime_id_seq OWNER TO postgres;

--
-- Name: runtime_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.runtime_id_seq OWNED BY public.runtime.id;


--
-- Name: success_info; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.success_info (
    id bigint NOT NULL,
    call_id character varying(100) DEFAULT ''::character varying,
    content character varying(300) DEFAULT ''::character varying,
    d_time timestamp without time zone DEFAULT now(),
    is_read boolean DEFAULT false
);


ALTER TABLE public.success_info OWNER TO postgres;

--
-- Name: success_info_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.success_info_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.success_info_id_seq OWNER TO postgres;

--
-- Name: success_info_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.success_info_id_seq OWNED BY public.success_info.id;


--
-- Name: success_keys; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.success_keys (
    id bigint NOT NULL,
    keyword character varying(100) NOT NULL,
    description text DEFAULT ''::text
);


ALTER TABLE public.success_keys OWNER TO postgres;

--
-- Name: success_keys_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.success_keys_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.success_keys_id_seq OWNER TO postgres;

--
-- Name: success_keys_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.success_keys_id_seq OWNED BY public.success_keys.id;


--
-- Name: dsr_cdr id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.dsr_cdr ALTER COLUMN id SET DEFAULT nextval('public.ai_cdr_id_seq'::regclass);


--
-- Name: dsr_flow_content id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.dsr_flow_content ALTER COLUMN id SET DEFAULT nextval('public.flow_content_id_sqe'::regclass);


--
-- Name: dsr_user id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.dsr_user ALTER COLUMN id SET DEFAULT nextval('public.dsr_user_id_seq'::regclass);


--
-- Name: fail_keys id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.fail_keys ALTER COLUMN id SET DEFAULT nextval('public.fail_keys_id_seq'::regclass);


--
-- Name: fine_keys id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.fine_keys ALTER COLUMN id SET DEFAULT nextval('public.fine_keys_id_seq'::regclass);


--
-- Name: runtime id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.runtime ALTER COLUMN id SET DEFAULT nextval('public.runtime_id_seq'::regclass);


--
-- Name: success_info id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.success_info ALTER COLUMN id SET DEFAULT nextval('public.success_info_id_seq'::regclass);


--
-- Name: success_keys id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.success_keys ALTER COLUMN id SET DEFAULT nextval('public.success_keys_id_seq'::regclass);


--
-- Name: ai_cdr_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.ai_cdr_id_seq', 73, true);


--
-- Data for Name: call_pg_cdr; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.call_pg_cdr (id, local_ip_v4, caller_id_name, caller_id_number, outbound_caller_id_number, destination_number, context, start_stamp, answer_stamp, end_stamp, duration, billsec, hangup_cause, uuid, bleg_uuid, accountcode, read_codec, write_codec, record_file, direction, sip_hangup_disposition, origination_uuid, sip_gateway_name, sip_term_status, sip_term_cause) FROM stdin;
\.


--
-- Name: call_pg_cdr_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.call_pg_cdr_id_seq', 50834, true);


--
-- Data for Name: dsr_cdr; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.dsr_cdr (id, account_id, callee, caller, start_time, end_time, duration, record_base, record_path, call_id, a_answer_time, b_answer_time, a_billsec, b_billsec, b_caller, b_callee, other_uuid, a_score, b_score, score, offline_qc) FROM stdin;
\.


--
-- Data for Name: dsr_flow_content; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.dsr_flow_content (id, call_id, content, record_path, record_base, callee, caller, dsr_time, fine, fail, speak_second) FROM stdin;
\.


--
-- Data for Name: dsr_seat; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.dsr_seat (id) FROM stdin;
\.


--
-- Data for Name: dsr_user; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.dsr_user (id, account, password, name, salt, create_time) FROM stdin;
2	test1	945c65b120a8e09f1fa507128e9ba48b	123	szFswPLYXE	1542347856
1	admin	945c65b120a8e09f1fa507128e9ba48b	超级管理员	szFswPLYXE	1542347201
\.


--
-- Name: dsr_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.dsr_user_id_seq', 8, true);


--
-- Data for Name: fail_info; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.fail_info (id, call_id, content, d_time, is_read) FROM stdin;
1	36d7f550-e71b-11e8-90d8-6f6819c8a509	滚\n	2018-11-13 16:07:36.827073	f
2	36d7f550-e71b-11e8-90d8-6f6819c8a509	骗子\n	2018-11-13 16:07:38.936549	f
3	123123	我	2019-08-19 16:01:28.818915	f
3	2d47c2d8-8a9d-11ee-888c-c7588e0b6fd3	"你是个骗子吗?"\n	2023-11-24 15:44:43.837493	t
4	2d47c2d8-8a9d-11ee-888c-c7588e0b6fd3	"你是个骗子吗?"\n	2023-11-24 15:44:44.223128	f
5	2d47c2d8-8a9d-11ee-888c-c7588e0b6fd3	"滚犊子。"\n	2023-11-24 15:44:59.670917	f
6	6788071e-8aa2-11ee-8429-c7588e0b6fd3	"不的意的,他妈的。"\n	2023-11-24 16:21:21.095665	f
7	6788071e-8aa2-11ee-8429-c7588e0b6fd3	"为什么老是电话轰炸我?"\n	2023-11-24 16:21:28.181796	t
8	6788071e-8aa2-11ee-8429-c7588e0b6fd3	"能不能不要再给我打电话了?"\n	2023-11-24 16:21:32.654242	t
9	6788071e-8aa2-11ee-8429-c7588e0b6fd3	"能不能不要再给我打电话了?"\n	2023-11-24 16:21:32.820054	f
10	6788071e-8aa2-11ee-8429-c7588e0b6fd3	"好多骗子。"\n	2023-11-24 16:21:36.46364	t
11	d830f23a-8aa4-11ee-ab71-c7588e0b6fd3	"来改了以后再没测过他妈的这个东西是要客户选择。"\n	2023-11-24 16:39:51.804281	t
12	d830f23a-8aa4-11ee-ab71-c7588e0b6fd3	"然后比如说这个就说是什么指定账号啊,或者说是报警啊,他就实时的就那个变成颜色嘛。"\n	2023-11-24 16:43:54.95453	t
13	d830f23a-8aa4-11ee-ab71-c7588e0b6fd3	"第这个对这个都通过关键字,或者说是其他的一些东西,我们就可以把它给检测出来了。你可以直接就关机了,对,应该想跟该挂机,直接在这个地方,比如说你打钱给我嗯打到指定账号里。"\n	2023-11-24 16:44:17.135847	t
14	d830f23a-8aa4-11ee-ab71-c7588e0b6fd3	"你训练没有意义,这玩意他妈的嗯,上次有个哥们是嗯白嫖了个a100 。"\n	2023-11-24 16:48:40.034983	t
15	2f878a58-8ac8-11ee-84e4-c7588e0b6fd3	"这个东西是不是个骗子?"\n	2023-11-24 20:51:44.421766	f
16	2f878a58-8ac8-11ee-84e4-c7588e0b6fd3	"是不是骗子?"\n	2023-11-24 20:51:55.413913	f
17	89a37aec-8ac8-11ee-b264-c7588e0b6fd3	"这是不是个骗子?"\n	2023-11-24 20:54:25.534095	f
18	360666be-8ac9-11ee-8f5d-c7588e0b6fd3	"你们是不是骗子?"\n	2023-11-24 20:59:13.296649	f
19	abc7c98e-8acd-11ee-aa9b-c7588e0b6fd3	"滚你妈的。"\n	2023-11-24 21:31:07.906143	f
\.


--
-- Name: fail_info_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.fail_info_id_seq', 19, true);


--
-- Data for Name: fail_keys; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.fail_keys (id, keyword, description, score, auto_hangup) FROM stdin;
11	假货		0	f
12	你大爷的		0	f
13	指定账号		0	f
14	突发疾病		0	f
16	傻屌		0	f
17	去你妈		0	f
18	他妈的		0	f
19	你妈个逼		0	f
20	他妈个逼		0	f
21	操你妈		0	f
22	你他妈的		0	f
23	你妈了个逼		0	f
24	脑子有病		0	f
25	生儿子没屁眼		0	f
26	操你大爷		0	f
27	日你妈		0	f
28	我是你爸爸		0	f
29	我是你爷爷		0	f
30	你是我儿子		0	f
31	滚		0	f
32	滚蛋		0	f
33	你妈的		0	f
34	二逼		0	f
35	去死		0	f
36	我操		0	f
37	屌毛		0	f
38	死了		0	f
39	全家死了		0	f
40	傻逼		0	f
41	弱智		0	f
42	脑瘫		0	f
43	脑残		0	f
44	我是你爹		0	f
45	二货		0	f
46	蠢货		0	f
47	蠢蛋		0	f
48	神经病		0	f
49	傻蛋		0	f
50	扑街		0	f
51	笨蛋		0	f
52	废物		0	f
53	白痴		0	f
54	三八		0	f
55	无赖		0	f
56	智障		0	f
57	王八		0	f
58	猪		0	f
59	畜生		0	f
60	贱人		0	f
61	小兔崽子		0	f
62	妈卖批		0	f
63	禽兽		0	f
64	低能		0	f
65	傻子		0	f
66	坏蛋		0	f
67	坏人		0	f
68	小人		0	f
69	\n\n二次检测		0	f
70	指定账号		0	f
71	清理微信		0	f
72	突发疾病		0	f
73	高收益		0	f
74	募捐		0	f
75	赚大钱		0	f
76	转点钱		0	f
77	重新激活		0	f
78	中奖频率		0	f
79	制毒		0	f
80	账号和密码		0	f
81	账号被封		0	f
82	账号被冻		0	f
83	长期贷款		0	f
84	月息低		0	f
85	银行系统		0	f
86	\n银行错误		0	f
87	一起投资		0	f
88	一起捞回		0	f
89	寻求帮助		0	f
90	幸运观众		0	f
91	信息不全		0	f
92	洗钱		0	f
93	无担保		0	f
94	稳赚不赔		0	f
95	网站漏洞		0	f
96	网银升级		0	f
97	网上兼职		0	f
98	突发急性病		0	f
99	提供详细信息		0	f
100	撕票		0	f
101	\n双倍赔偿		0	f
102	双倍理赔		0	f
103	刷信誉		0	f
104	赎金		0	f
105	受人所托		0	f
106	手机赚外快		0	f
107	删掉聊天记录		0	f
108	日赚		0	f
109	清除聊天记录		0	f
110	签收单看不清		0	f
111	破财消灾		0	f
112	破财免灾		0	f
113	嫖娼被抓		0	f
114	免信用调查		0	f
115	\n每周能赚		0	f
116	聊天记录删掉		0	f
117	聊天记录删除		0	f
118	快递丢了		0	f
119	巨额奖品		0	f
120	救助金		0	f
121	缴关税		0	f
122	好友删除		0	f
123	航班取消		0	f
124	查扣		0	f
125	国家账户		0	f
126	公证费		0	f
127	高额信用卡		0	f
128	改签或退票		0	f
129	复制手机卡		0	f
130	\n服装费		0	f
131	飞机故障		0	f
132	发生车祸		0	f
133	发财致富		0	f
134	二次实名认证		0	f
135	电子请柬		0	f
136	点赞有奖		0	f
137	代为保管		0	f
138	出售游戏币		0	f
139	茶叶滞销		0	f
140	操作失误		0	f
141	藏品		0	f
142	便于送货上门		0	f
143	安全账户		0	f
144	\n爱心传递		0	f
145	ATM办理		0	f
146	通管局		0	f
147	经侦		0	f
2	骗子		0	t
15	报警		0	t
148	客服QQ		0	f
149	习近平		0	f
150	胡锦涛		0	f
151	江泽民		0	f
152	刑侦队		0	f
153	线上操作工		0	f
154	工作简单易懂		0	f
155	线上操作员		0	f
156	男士服务		0	f
157	时时彩		0	f
158	线上跟单员		0	f
159	兑换积分		0	f
160	\n投资理财		0	f
161	免费水果		0	f
162	电商联盟		0	f
163	手机免提		0	f
164	反馈活动		0	f
165	京东商城		0	f
166	京东金融		0	f
167	贷款注销		0	f
168	武警部		0	f
169	后勤部采购		0	f
170	兼职人员		0	f
171	京东客服		0	f
172	理赔		0	f
173	猜猜我是谁		0	f
174	回馈		0	f
175	免费派送		0	f
176	\n庆祝		0	f
177	免费礼品		0	f
178	淘宝商家联盟		0	f
179	回馈		0	f
180	淘宝商家联盟		0	f
181	中奖		0	f
182	加微信		0	f
183	免费派送		0	f
184	免费领取		0	f
185	消防队		0	f
186	武警		0	f
187	部队		0	f
188	订餐		0	f
189	采购		0	f
190	银保监会		0	f
191	京东客服		0	f
192	京东白条		0	f
193	京东金条		0	f
194	\n京东金融		0	f
195	大鹏教育		0	f
196	退费		0	f
197	学费清退		0	f
198	美团优选		0	f
199	淘宝商家联盟		0	f
200	淘宝电商联盟		0	f
201	淘宝电商协会		0	f
202	淘宝联盟商家		0	f
203	天猫客服		0	f
204	天猫超市客服		0	f
205	浙江商会		0	f
206	关闭微粒贷窗口		0	f
207	\n支付宝蚂蚁金服		0	f
208	机票航班取消		0	f
209	改签		0	f
210	推票		0	f
211	领取红包		0	f
212	58招聘加qq		0	f
213	免费派送		0	f
214	免费赠送		0	f
215	\n免费办理ETC		0	f
216	POS机刷卡套现		0	f
217	存款高利率		0	f
218	境外高薪就业		0	f
219	网上赌博		0	f
220	厂家直销低价净水器		0	f
221	彩票中奖		0	f
222	淘宝刷单返利		0	f
223	\n报警		0	f
224	投诉你		0	f
225	举报你		0	f
226	烦死		0	f
227	烦死了		0	f
228	别烦我		0	f
229	打110		0	f
230	能不能不要打电话		0	f
231	三天两头打电话		0	f
232	不要老打电话		0	f
233	不要打电话		0	f
234	老是打来		0	f
235	别打来了		0	f
236	\n老是打电话		0	f
237	怎么知道号码		0	f
238	天天打电话		0	f
239	有人骚扰我		0	f
240	怎么知道我的号码		0	f
241	打电话有毛病		0	f
242	那么多骗子		0	f
243	这么多骗子		0	f
244	好多骗子		0	f
245	从哪弄的信息		0	f
246	\n不要再给我打		0	f
247	怎么有我号码的		0	f
248	诈骗电话		0	f
249	电话轰炸		0	f
250	电话骚扰人家		0	f
251	不要再给我打电话了		0	f
252	能不能不要再给我打电话了		0	f
1	滚.*妈		0	t
\.


--
-- Name: fail_keys_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.fail_keys_id_seq', 252, true);


--
-- Data for Name: fine_info; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.fine_info (id, call_id, content, d_time, is_read) FROM stdin;
\.


--
-- Name: fine_info_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.fine_info_id_seq', 2, false);


--
-- Data for Name: fine_keys; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.fine_keys (id, keyword, description, score) FROM stdin;
1	产品很棒		0
\.


--
-- Name: fine_keys_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.fine_keys_id_seq', 1, true);


--
-- Name: flow_content_id_sqe; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.flow_content_id_sqe', 907, true);


--
-- Data for Name: runtime; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.runtime (id, call_id, a_number, b_number) FROM stdin;
41	d8805376-8b20-11ee-85ba-c7588e0b6fd3	18621575908	1000
\.


--
-- Name: runtime_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.runtime_id_seq', 41, true);


--
-- Data for Name: success_info; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.success_info (id, call_id, content, d_time, is_read) FROM stdin;
1	8502e102-e713-11e8-81e6-6f6819c8a509	["好的好的，"]\n	2018-11-13 15:12:34.355085	f
2	8502e102-e713-11e8-81e6-6f6819c8a509	["好的好的，"]\n	2018-11-13 15:12:37.333899	f
3	95fa0444-e714-11e8-94bc-6f6819c8a509	["好的，点，"]\n	2018-11-13 15:20:13.96074	t
4	95fa0444-e714-11e8-94bc-6f6819c8a509	["好的好的，可以，"]\n	2018-11-13 15:20:15.165727	f
5	56f581e4-ebd1-11e8-bb2c-d71d629bc6ce	["喂喂喂你好，"]\n	2018-11-19 16:01:56.078241	f
6	56f581e4-ebd1-11e8-bb2c-d71d629bc6ce	["啊，现在好的有好多了不是很卡，"]\n	2018-11-19 16:02:06.811141	f
7	56f581e4-ebd1-11e8-bb2c-d71d629bc6ce	["那好呀，"]\n	2018-11-19 16:02:23.920654	t
8	101f10e8-fd0e-11e8-b7da-8f13701cd70b	["好，"]\n	2018-12-11 14:28:06.680443	t
9	39ce39c8-fd0e-11e8-851b-8f13701cd70b	["啊好的好的，"]\n	2018-12-11 14:29:42.621461	t
10	39ce39c8-fd0e-11e8-851b-8f13701cd70b	["好点盐多少钱，"]\n	2018-12-11 14:29:49.819882	f
11	94e0bab6-fd0e-11e8-8fee-935ff44a66ec	["好像没颜色，"]\n	2018-12-11 14:32:03.159669	f
12	94e0bab6-fd0e-11e8-8fee-935ff44a66ec	["二好的好像又好了好的嘛我在走廊那边的话，"]\n	2018-12-11 14:32:31.790868	f
13	8070ce63ee61490898c57eadf243bd6d	但是现在好像我这边有自己的回音啊。	2018-12-11 14:43:27.443532	f
14	8070ce63ee61490898c57eadf243bd6d	rap得啊，那目前撤下来两只排水还比较良好的。	2018-12-11 14:43:41.030151	f
15	8070ce63ee61490898c57eadf243bd6d	嗯，但是现在好像又有一点那个延迟了，好像又是200，和你才是给他说话。	2018-12-11 14:43:58.521052	f
16	8070ce63ee61490898c57eadf243bd6d	我这边感觉还行。	2018-12-11 14:44:09.182006	f
17	erApKrZ3A5	反正我也不知道有没有下雨，好像好像没有了吧。	2018-12-11 17:49:07.722265	f
18	ef36ad76de3c497db26a390d574da50c	好像现在不下雨了。	2018-12-11 17:55:11.253696	f
19	ef36ad76de3c497db26a390d574da50c	现在好像不下雨了。	2018-12-11 17:55:15.143263	f
20	d830f23a-8aa4-11ee-ab71-c7588e0b6fd3	"不过大部分的场景还行了,这只只是个助手啊,反正只是个助手。"\n	2023-11-24 16:41:01.047986	f
\.


--
-- Name: success_info_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.success_info_id_seq', 20, true);


--
-- Data for Name: success_keys; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.success_keys (id, keyword, description) FROM stdin;
4	还行	
5	可以吧	
9	一般啦	
\.


--
-- Name: success_keys_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.success_keys_id_seq', 10, true);


--
-- Name: call_pg_cdr call_pg_cdr_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.call_pg_cdr
    ADD CONSTRAINT call_pg_cdr_pkey PRIMARY KEY (id);


--
-- Name: dsr_cdr dsr_cdr_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.dsr_cdr
    ADD CONSTRAINT dsr_cdr_pkey PRIMARY KEY (id);


--
-- Name: dsr_flow_content dsr_flow_content_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.dsr_flow_content
    ADD CONSTRAINT dsr_flow_content_pkey PRIMARY KEY (id);


--
-- Name: dsr_seat dsr_seat_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.dsr_seat
    ADD CONSTRAINT dsr_seat_pkey PRIMARY KEY (id);


--
-- Name: dsr_user dsr_user_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.dsr_user
    ADD CONSTRAINT dsr_user_pkey PRIMARY KEY (id);


--
-- Name: runtime runtime_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.runtime
    ADD CONSTRAINT runtime_pkey PRIMARY KEY (id);


--
-- Name: callee_number_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX callee_number_index ON public.call_pg_cdr USING btree (destination_number);


--
-- Name: caller_number_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX caller_number_index ON public.call_pg_cdr USING btree (caller_id_name, caller_id_number);


--
-- Name: pg_cdr_uuid_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX pg_cdr_uuid_idx ON public.call_pg_cdr USING btree (uuid, bleg_uuid);


--
-- Name: start_time_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX start_time_index ON public.call_pg_cdr USING btree (start_stamp);


--
-- PostgreSQL database dump complete
--

