&emsp;&emsp;主要是用于实时质检或以前所谓的座席助手等应用。每台服务器免费是限制20 channels。

&emsp;&emsp; mod_dsr(双路语音流), 指的是来自两个音源的同时音频流。例如，两个参与者通过电话、视频会议或其他实时语音通话工具进行对话。在这种情况下，系统需要同时处理并识别两路音频流中的语音内容。

&emsp;&emsp;mod_dsr成型于2018年5月左右，2018年10月发布了第一个版本，但因为以前都是配合客户，相当于我们把短句等推送到客户侧，由客户侧再进行识别和处理，所以我们的页面功能等，其实大部分客户是根本看都不会看。

&emsp;&emsp;mod_dsr的开发宗旨，第一、成本不可太高，毕竟当年一套离线部署的asr，对于中小型公司的代价和成本过于高。第二、面向大家一般性的聊天沟通，进行实时的文字转写、记录。第三、面对一些异常状态的对话，可以实时挂断通话，从而避免某些事态的进一步扩大。

```
目录结构
├─auto_add_words                                             自动添加一些关键词进相关的库里
├─bd_asr                                                     2018年实现的调用百度api的一句话识别源码
├─databases                                                  数据库表结构，需要导入到postgresql中
├─dsr_web                                                    web管理部分源码，只起管理和查看作用，实际运行的状态不在这里
│  ├─conf
│  ├─controllers
│  ├─libs
│  ├─models
│  ├─msc
│  │  └─4fd8751ee34fc42fc62cd50a3487bcef
│  ├─routers
│  ├─static
│  ├─utils
│  │  ├─conf
│  │  ├─eventsocket
│  │  ├─export_excel
│  │  ├─log
│  │  ├─nway_datetime
│  │  ├─nway_db_connection
│  │  ├─nway_ip
│  │  ├─nway_office
│  │  ├─nway_path
│  │  ├─nway_print
│  │  ├─nway_string
│  │  └─nway_uuid
│  └─views
│      ├─admin
│      ├─cdr
│      ├─home
│      ├─keywords
│      ├─login
│      └─main
└─nway_dsr                                                   一直秉承是应用和web分离，此应用用于freeswitch间打交道，记录断句以及进行转写等                                           
│    ├─nway_fs_ctrl
│    ├─nway_fs_db
│    └─utils
│        ├─conf
│        ├─eventsocket
│        ├─log
│        ├─nway_datetime
│        ├─nway_db_connection
│        ├─nway_ip
│        ├─nway_office
│        ├─nway_path
│        ├─nway_print
│        ├─nway_string
│        └─nway_uuid
│─mod_dsr
│  ├─cdr_pg_csv.conf.xml                                     mod_cdr_pg_csv的配置，我们的话单依赖于它
│  ├─dsr.conf.xml                                             dsr.conf.xml  模块的配置需要置于:$FS_PATH/autoload_configs/下
│  ├─libnway_auth_lib.so                                     需置于$FS_PATH/lib/下
│  ├─license.txt                                             一个默认的授权文件，置于/opt/nway_dsr/license.txt
│  ├─mod_dsr.so                                              主模块，需置于$FS_PATH/mod/下
       
```        

dsr配置文件
```
<configuration name="dsr.conf" description="DSR Configuration">
  <settings>
    <param name="recording-dir" value="/usr/local/freeswitch/recordings/"/>
    <param name="silence-duration" value="10000"/>
    <param name="license-file" value="/opt/nway_dsr/license.txt"/>
    <param name="log_dir" value="/tmp/"/>
	  <param name="dsr-leg" value="ab"/>   
	  <!--  dsr-leg ab or a or b -->
	  <param name="dsr-scale" value="1"/>
	  <!-- dsr-scale 质检范围 1为 1000% 2为每两个选一个，5为每5个选一个 -->
    <param name="dsr-burst" value="1"/>
    <!-- dsr-burst 分段对一个声道进行质检，如果为0，则一个声道只存一个文件-->
   </settings>
</configuration>
```

路由配置文件部分如下：
```
     <extension name="Public_lihao">
        <condition field="destination_number" expression="^(.*)$">
            <action application="export" data="exten_record_file=$${recordings_dir}/${strftime(%Y-%m-%d)}/${caller_id_number}.$1.${strftime(%H-%M-%S)}.${uuid}.wav"/>
             <action application="set" data="record_file=${exten_record_file}"/>
            <action application="set" data="call_timeout=30"/>
            <action application="set" data="continue_on_fail=true"/>
            <action application="set" data="dtmf_type=info"/>
            <action application="export" data="hangup_after_bridge=true"/>
            <action application="export" data="return_ring_ready=true"/>
            <action application="export" data="bridge_early_media=true"/>
            <action application="export" data="sip_auto_answer=true"/>
             
            <action application="bridge" data="{execute_on_answer='dsr ${record_file}'}user/1000"/>

        </condition>
    </extension>
```

登录时是采用：IP:8098 登录，用户名：admin,密码: 123456  。 在进行通话时，我们就会实时把一些内容展现出来，见图4和图5，虽然列的是通话记录中的，但是实时通话时，它本身就是如此的。

![输入图片说明](pics/1.png)
图1. 配置骂人等不友好的词汇

![输入图片说明](pics/2.png)
图2. 配置成交等对于通话中可以加分的词汇

![输入图片说明](pics/3.png)
图3. 可以按时间段查询对话

![输入图片说明](pics/4.png)
图4. 类似于有意向的用户等对话

![输入图片说明](pics/5.png)
图5. 类似于骂人等的对话